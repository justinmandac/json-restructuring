#!/usr/bin/python
import json
import sys

# returns a new array of employees that have been cleared of ones having status == 2
def purgeList (plist):
    purge = []
    for person in plist:
        if person["status"] == 2: #remove ended employees
            #since there exists a use case for orphaned employees:

            #iterate 'person' through the provided array and find the elements that will be
            #orphaned.
            # [1] clear the manager_name property to signify that its parent has
            #     been purged from the list
            for index, cl in enumerate(filter(lambda rem: rem["manager_name"] == person["login_name"], plist)):
                cl["manager_name"] = "" #[1]
                purge.append(cl)

        else:
            #simply add items w/ status == 1
            purge.append(person)
    return purge

#acquires subordinates for a given manager_name - mname
def getManagerObjects (mname, plist) :
    mobj = {
        "name" : mname,
        "subordinate" : []
    }
    #get subordinates
    # iterate mname through the list;
    for index, sub in enumerate(plist):
        # mname is someone's manager, ergo get any child nodes that may be present
        if sub["manager_name"] == mname:
            if sub["status"] == 1: #ignore inactive ones
                mobj["subordinate"].append(getManagerObjects(sub["login_name"], plist))
        elif len(sub["manager_name"]) == 0 and sub["status"] == 1: # process orphaned employees
            plist.pop(plist.index(sub))
            mobj["subordinate"].append(getManagerObjects(sub["login_name"], plist))

    # terminating condition
    #clean-up if there are no subordinates for mname
    if (len(mobj["subordinate"]) == 0):
        del mobj["subordinate"]
    return mobj


outfile = ""
infile  = ""

if len(sys.argv) < 3:
    print("Insufficient arguments. Make sure that you invoked the script correctly.")
    print("python main.py <input file> <output file>")
    sys.exit()
else:
    infile  = sys.argv[1]
    outfile = sys.argv[2]

#json_list = data.jsonObj
json_list = json.load(open(infile, 'r'))

#get the list of employed managers. this list will act as the collection
#of the outermost nodes of the resulting JSON
managersList = set(filter( #ensure uniqueness of array
                lambda x: len(x) > 0, #filter out blank values, ie do not include orphaned nodes
                    map(lambda x: x["manager_name"],
                        purgeList(json_list)))) #remove items with status == 2

managers = [getManagerObjects(m, json_list) for m in managersList]

with open(outfile, 'w') as outfile:
     json.dump(managers, outfile, sort_keys = True, indent = 2,
    ensure_ascii=False)
